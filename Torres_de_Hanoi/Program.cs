﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Torres_de_Hanoi
{
    class Program
    {
        static void Main(string[] args)
        {


            Console.WriteLine("SOLUCIONADOR DE TORRES DE HANOI");
            int qty = 1; char method = 'I'; //Configuración por defecto, 1 disco y resolución iterativa

            while (true) {

                //Se pide el número de discos al usuario
                Console.WriteLine("");
                Console.WriteLine("Con cuantos discos desea resolver el problema?");
                String input = Console.ReadLine();
                Int32.TryParse(input, out qty);

                //Se pregunta por el método de resolución deseado
                Console.WriteLine("");
                Console.WriteLine("Si desea resolverlo de forma iterativa introduzca una I, o si desea resolverlo de forma recursiva una R");
                input = Console.ReadLine();
                Char.TryParse(input, out method);

                //Creación de las pilas
                Pila p_ini = new Pila(qty, "INI"); 
                Pila p_aux = new Pila(0, "AUX");  
                Pila p_fin = new Pila(0, "FIN");
                
                Hanoi h = new Hanoi();


                if (method.CompareTo('I') == 0) { 
                    //Resolución iterativa
                    Console.WriteLine("Resolviendo de forma iterativa para " + qty + " discos...");
                    Console.WriteLine("----------------------------------------------------------");
                    h.iterativo(qty, p_ini, p_fin, p_aux);
                } else if(method.CompareTo('R') == 0) {
                    Console.WriteLine("Resolviendo de forma recursiva para " + qty + " discos...");
                    Console.WriteLine("----------------------------------------------------------");
                    h.recursivo(qty, p_ini, p_fin, p_aux);
                }
                else { Console.WriteLine("No se ha introducido un caracter válido para determinar el método de resolución!"); }

                Console.WriteLine("Presiona Q para quitar o cualquier tecla para volver a resolver...");
                String replay = Console.ReadLine();
                if(replay == "Q" || replay=="q") { break; }

                }

            Environment.Exit(0); //Cierra la aplicación

        }
    }
}
