﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Torres_de_Hanoi
{
    class Pila
    {
        public int Size { get; set; }
        
        //Disco más alto de la pila
        public int Top { get; set; }

        //Listado de discos

        public String Nombre { get; set; }
        public List<Disco> Elementos { get; set; }

        /* TODO: Elegir tipo de Elementos
          public Disco[] Elementos { get; set; }
        */

        /* TODO: Implementar métodos */



        /*
         * Se define la capacidad de la pila al crearlo
         * Y se le da un nombre para que sea reconocible la pila
         */
        public Pila(int Size, String nombre)
        {

            // Se instancian la capacidad, el listado y un nombre para la pila
            this.Size = Size;
            this.Nombre = nombre;
            this.Elementos = new List<Disco>();

            if (this.Size > 0) { 
                //Se llena la pila con el numero de discos correspondientes
                for (int i = this.Size; i > 0; i--) {
                    Disco d = new Disco(); d.Valor = i;
                    this.Elementos.Add(d);
                }

                this.Top = this.Elementos[this.Size-1].Valor;   //Se actualiza el disco que ocupa la parte superior

            }
            else { this.Top = 0; }

            //Console.WriteLine("Disco Top " + this.Top);

        }

        /*
        * Se inserta un disco en la pila, y se actualiza la cantidad de discos que esta contiene
        */
        public void push(Disco d)
        {
            this.Elementos.Add(d);
            this.Size++;

            this.Top = d.Valor;
        }


        /*
         * Se coje el disco mas alto, se elimina del listado de discos de la pila
         * se actualiza la cantidad de discos de la pila, y se devuelve por parametro
         */
        public Disco pop()
        {
            Disco d = null;
            if (this.Size >= 1) {
                d = this.Elementos[this.Size - 1];
                this.Elementos.RemoveAt(this.Size - 1);
                this.Size--;

                if (this.Size > 0){
                    this.Top = this.Elementos[this.Size - 1].Valor;
                } else { this.Top = 0; }
            }


            return d;
        }


        /*
         * Se comprueba si la pila esta vacia
         */
        public bool isEmpty()
        {
            bool empty = true;
            if (this.Size > 0) { empty = false; }
            else{
                empty = true;
            }

            return empty;
        }

    }
}
