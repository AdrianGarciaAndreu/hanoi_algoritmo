﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Torres_de_Hanoi
{
    class Hanoi
    {

        private int m = 0;
        private bool printed = false;

        /*TODO: Implementar métodos*/
        public int mover_disco(Pila a, Pila b)
        {

            //Almaceno si el movimiento se realiza o no
            String origen, destino;
            String origenNombre, destinoNombre;
            int m = 0;

            if (a.isEmpty() && b.isEmpty()){} //No hay movimiento posible
            else 
            {

                m = 1; //Hay movimiento
                //Mueve de A a B
                if (!a.isEmpty() && ((a.Top < b.Top) || ((a.Top > b.Top) && (b.isEmpty())))){
                   
                    origen = Convert.ToString(a.Top); destino = Convert.ToString(b.Top); //Texto para mostrar por pantalla
                    origenNombre = a.Nombre; destinoNombre = b.Nombre;
                    if(b.Top== 0) { destino = "VACÍO";  }

                    Disco d = a.pop();b.push(d); //Muevo el disco
                }
                else {
                    //De B a A

                    origen = Convert.ToString(b.Top); destino = Convert.ToString(a.Top); //Texto para mostrar por pantalla
                    origenNombre = b.Nombre; destinoNombre = a.Nombre;
                    if (a.Top == 0) { destino = "VACÍO"; }

                    Disco d = b.pop(); a.push(d); //Muevo el disco
                }

                //Muestra mensaje del movimiento realizado al usuario
                Console.WriteLine("Mueve de " + origenNombre + " (" + origen + ") a " + destinoNombre + " (" + destino + ")");

            }



            return m;
        }


        public bool gameFinished(Pila ini, Pila fin, Pila aux)
        {
            bool result = false;
            if(ini.isEmpty() && aux.isEmpty()){
                for (int i = fin.Size; i > 0; i--){
                    if(fin.Elementos[i-1].Valor != i ) { result = false; }
                }
                result = true;
            }

            return result;

        }
        

        public int iterativo(int n, Pila ini, Pila fin, Pila aux)
        {

            if ((n % 2)!=0){ //Impar
                while(!gameFinished(ini, fin, aux)) 
                {
                    this.m+= mover_disco(ini, fin); if (ini.isEmpty() && aux.isEmpty()) { break; }
                    this.m += mover_disco(ini, aux);
                    this.m += mover_disco(aux, fin); if (ini.isEmpty() && aux.isEmpty()) { break; }
                    Console.WriteLine("");
                }

            }
            else
            { //Par
                while (!gameFinished(ini, fin, aux))
                {
                    this.m += mover_disco(ini, aux);
                    this.m += mover_disco(ini, fin); if (ini.isEmpty() && aux.isEmpty()) { break; }
                    this.m += mover_disco(aux, fin); if (ini.isEmpty() && aux.isEmpty()) { break; }
                    Console.WriteLine("");

                }



            }

            Console.WriteLine("\nresuelto de forma iterativa en " + m+ " MOVIMIENTOS! ");
            Console.WriteLine("----------------------------------------------------------");
            return m;
        }


        
        public int recursivo(int n, Pila ini, Pila fin, Pila aux)
        {
            bool ended = false;

            //while (!gameFinished(ini, fin, aux)) { 
            if (!ended) { 
                if (n == 1){ this.m += mover_disco(ini, fin); } 
                else {
                    recursivo((n-1), ini, aux, fin);
                    this.m += mover_disco(ini, fin);
                    recursivo((n-1), aux, fin, ini);
                }
            }

            ended = gameFinished(ini, fin, aux);
            if (ended && !this.printed) {
                this.printed = true;
                Console.WriteLine("\nresuelto de forma recursiva en " + m + " MOVIMIENTOS! ");
                Console.WriteLine("----------------------------------------------------------");
            }

            //} //Fin de la comprobación de juego terminado


            //Console.WriteLine("\nresuelto de forma recursiva en " + m + " MOVIMIENTOS! ");
            //Console.WriteLine("----------------------------------------------------------");

            return m;
        }



    }
}
